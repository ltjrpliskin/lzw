#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>

#include "encode.h"
extern "C" {
	#include "decode.h"
}

int main(int argc, char *argv[]) {
	using namespace std;
	
	if ( (argc < 3) || (argc > 4) ) {
		fprintf(stderr, "useage: %s encode/decode inFilename [outFilename]\n", argv[0]);
		return 1;
	}
	
	bool success = false;
	
	if (strcmp(argv[1], "encode") == 0) {
		
		// when no outfile name
		if (argc == 3) {					
			string outname(argv[2]);
			outname += ".z";
			success = Encode(argv[2], outname.c_str());
		}
	
		// when outfile name specified 
		else 
			success = Encode(argv[2], argv[3]);
	}
	
	else if (strcmp(argv[1], "decode") == 0) {
		
		// when no outfile name
		if (argc == 3) {					
			string outname(argv[2]);
			outname += ".txt";
			success = Decode(argv[2], outname.c_str());
		}
	
		// when outfile name specified 
		else 
			success = Decode(argv[2], argv[3]);		
	}
	
	else {
		fprintf(stderr, "useage: %s encode/decode inFilename [outFilename]\n", argv[0]);
		return 1;
	}
	

	if (success)
		return 0;
	else 
		return 1;
}
