#include <cstdio>
#include <iostream>
#include <cstdint>
#include <vector>
#include <string>
#include "encode.h"
using namespace std;

void InitTable(vector<entry> *table) {
	
	entry tmpEntry;
	
	for (uint16_t i = 0; i < 256; i++) {
		tmpEntry.code = i;
		tmpEntry.word = (uint8_t) i;
		table[i].push_back(tmpEntry);
	}
}

void AddToTable(vector<entry> *table, string wordToAdd, CodeInfo *codePtr) {

	uint8_t pos = (uint8_t) wordToAdd[0];
	
	entry tmpEntry;
	tmpEntry.code = codePtr->currCode;
	tmpEntry.word = wordToAdd;
	
	table[pos].push_back(tmpEntry);
}

entry *SearchTable(vector<entry> *table, string wordToSearch) {

	uint8_t pos = (uint8_t) wordToSearch[0];
	uint16_t size = table[pos].size();
	
	for (uint16_t i = 0; i < size; i++) {
		
		if (table[pos][i].word.compare(wordToSearch) == 0) 
			return &table[pos][i];
	}
	
	return NULL;
}

void ResetTable(vector<entry> *table) {
	
	for (int i = 0; i < 256; i++)
		table[i].resize(1);

}

void PrintCode(CodeInfo *codePtr, FILE *outptr, uint16_t codeToPrint) {
	
	if (codePtr->currCode % 2 == 0) {
		codePtr->codeBuffer[0] = codeToPrint >> 4;
		codePtr->codeBuffer[1] = (codeToPrint & 0xf) << 4;
	}
	
	else {
		codePtr->codeBuffer[1] |= (codeToPrint & 0xf00) >> 8;
		codePtr->codeBuffer[2] = (codeToPrint & 0xffff);
		fwrite(codePtr->codeBuffer, 1, 3, outptr);
	}

	codePtr->totalCode++;
}

bool Encode(const char *inFile, const char *outFile) {
	
	FILE *inptr = fopen(inFile, "r" );
	if (inptr == NULL) {
		fprintf(stderr, "Could not open file to read!\n");
		return false;
	}

	FILE *outptr = fopen(outFile, "w");
	if (outptr == NULL) {
		fprintf(stderr, "Could not open file to write!\n");
		fclose(inptr);
		return false;
	}
	
	vector<entry> table[256];
	InitTable(table);
	
	char buffer[4096];	
	
	CodeInfo codeData;
	codeData.currCode = 256;
	codeData.totalCode = 0;		
	
	uint16_t code = 0, prevcode = 0;
	entry *ptr;
	int numRead;
	string word;
	word += (char) fgetc(inptr);
		
	while ((numRead = fread(buffer, 1, 4096, inptr)) != 0) {
	
		// checking for read error
		if (numRead != 4096) {
			
			if (ferror(inptr)) {
				fprintf(stderr, "Error occured when reading!\n");
				fclose(inptr);
				fclose(outptr);
				return false;
			}			
		}
		
		for (int i = 0; i < numRead; ) {
		
			//word is in table
			if ( (ptr = SearchTable(table, word)) != NULL) {
				word += buffer[i];
				code = ptr->code;
				i++;
			}
			
			//word not in table
			else {
				AddToTable(table, word, &codeData);
				
				PrintCode(&codeData, outptr, prevcode);
				codeData.currCode++;
				
				if (codeData.currCode == 4096) {
					ResetTable(table);
					codeData.currCode = 256;
				}
				
				word[0] = word[word.length() - 1];
				word.resize(1);							
			}
			
			prevcode = code;
		}
	}
	
	uint8_t tmp[2];
	
	if (codeData.totalCode % 2 == 0) {
		
		//last word is in table -- so after writing this code, have odd number.
		if ( (ptr = SearchTable(table, word)) != NULL) {
			tmp[0] = ptr->code >> 8;
			tmp[1] = ptr->code & 0xff;
			
			fwrite(tmp, sizeof(tmp[0]), sizeof(tmp), outptr);
		}
		
		// last word not in table -- need to write 2 more codes
		else {
			char last = word[word.length() - 1];
			word.pop_back();
			ptr = SearchTable(table, word);
			PrintCode(&codeData, outptr, ptr->code);
			PrintCode(&codeData, outptr, (uint16_t)last);		
		}
	}
	
	// codeBuffer is half full
	else {
		
		// last word is in table - so just print this code
		if ( (ptr = SearchTable(table, word)) != NULL) 
			PrintCode(&codeData, outptr, ptr->code);
		
		else {
			tmp[0] = word[word.length() - 1];
			word.pop_back();
			ptr = SearchTable(table, word);
			PrintCode(&codeData, outptr, ptr->code);
			fwrite(tmp, sizeof(tmp[0]), sizeof(tmp), outptr);
		}		
	}
	
	fclose(inptr);
	fclose(outptr);
	return true;
	
}
