# compiler to use
CC = g++

# flags to pass compiler
CPPFLAGS = -g	-O3	-std=c++11	-Wall	-Werror

# name for executable
EXE = lzw

# space-separated list of header files
HDRS = encode.h decode.h

# space-separated list of libraries, if any,
# each of which should be prefixed with -l
LIBS =

# space-separated list of source files
SRCS = encode.cpp lzw.cpp decode.c

# automatically generated list of object files
OBJS = encode.o lzw.o decode.o

# default target
$(EXE): $(OBJS) $(HDRS) Makefile
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LIBS)


# dependencies 
$(OBJS): $(HDRS) Makefile

# housekeeping
clean:
	rm -f core $(EXE) *.o
