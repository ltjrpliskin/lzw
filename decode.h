#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

// The number of bytes of an encoded file is either 3k or 3k + 2
// Need buffersize to be multiple of 3, choose pagesize * 3
#define BUFFERSIZE 12288

// max number of codes is 2^12
#define MAXSIZE 4096

#define ALPHABETSIZE 256

/* Structure that contains: the dictionary (this is just an array of strings),
 * 							the current index pointed to,
 * 							the next word to be added at current index.
 */
typedef struct DICTINFO {
	char *dictionary[MAXSIZE];
	int index;
	char *wordToAdd;
} DICTINFO;

/* Function: Initialise the dictionary being pointed to.
 */
bool Init(DICTINFO *dictPtr);

/* Function: Resets the dictionary from index = offset to MAXSIZE - 1.
 */
void Reset(DICTINFO *dictPtr, int offset);

/* Function: Creates a new word, copies dictionary[code] into new word.
 *			 Makes sure new word has enough space to append another character.
 *			 Returns false if it cannot create a new word.
 */
bool CreateNewWord(DICTINFO* dictPtr, uint16_t code);

/* Function: To the current wordToAdd, append first character of a word.
 *			 At the current index, add wordToAdd.
 */

void AddToDictionary(DICTINFO *dictPtr, char *aWord);

/* Function: Check if current index is greater than max size of dictionary,
 *			 if it is, reset index and dictionary to starting position.
 */

void CheckIndex(DICTINFO *dictPtr);

/* Function: reads in 2 contiguous bytes, then converts it into
 *			 the code which was written by the encoder.
 */
uint16_t ConvToCode(uint8_t firstByte, uint8_t secondByte, int Position);

/* Function: Reads NoOfBytes from buffer, 
 *			 Converts these bytes to code and outputs into outptr.
 *			 Adds new words to dictionary.
 */
bool Output(FILE *outptr, int NoOfBytes, char *buffer, DICTINFO *dictPtr);

/* Function: decodes inFile, and writes to outFile.
 */
bool Decode(const char *filename, const char *outname);

#ifdef __cplusplus
}
#endif
