#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "decode.h"

/* Function: Initialise the dictionary being pointed to.
 */
bool Init(DICTINFO *dictPtr) {
	
	for (int i = 0; i < MAXSIZE; i++) {		
			
		if (i > 255)
			dictPtr->dictionary[i] = NULL;
		
		else {
			dictPtr->dictionary[i] = (char*)malloc(2 * sizeof(char));
			if (dictPtr->dictionary[i] == NULL)
				return false;
			
			sprintf(dictPtr->dictionary[i], "%c", i);
		}
	}
	
	dictPtr->wordToAdd = NULL;
	dictPtr->index = 256;
	return true;
}

/* Function: reads in 2 contiguous bytes, then converts it into
 *			 the code which was written by the encoder.
 */
uint16_t ConvToCode(uint8_t firstByte, uint8_t secondByte, int partNo) {

	uint16_t code = 0;
	
	// converts the first 12 out of 16 bits into code(represented by the 1's):
	// 11111111 11110000
	if (partNo == 0) {
		code = firstByte;
		code <<= 4;
		code |= (secondByte & 0xf0) >> 4;
	}
	
	// converts the last 12 out of 16 bits into code(represented by the 1's):
	// 00001111 11111111
	else {
		code = (firstByte & 0x0f);
		code <<= 8;
		code |= secondByte;
	}
	
	return code;
}

/* Function: Resets the dictionary from index = offset to MAXSIZE - 1.
 */
void Reset(DICTINFO *dictPtr, int offset) {

	for (int i = offset; i < MAXSIZE; i++){
		free(dictPtr->dictionary[i]);
		dictPtr->dictionary[i] = NULL;
	}
}

/* Function: Creates a new word, copies dictionary[code] into new word.
 *			 Makes sure new word has enough space to append another character.
 *			 Returns false if it cannot create a new word.
 */
bool CreateNewWord(DICTINFO* dictPtr, uint16_t code) {
	
	if (dictPtr->dictionary[code]) {
		
		dictPtr->wordToAdd = (char*)malloc(strlen(dictPtr->dictionary[code]) + 2);
		if (dictPtr->wordToAdd == NULL)
			return false;	
		else		
			strcpy(dictPtr->wordToAdd, dictPtr->dictionary[code]);
		return true;
	}
	
	else {
		fprintf(stderr, "Tried to copy a word which is not in dictionary!\n");
		return false;
	}
}

/* Function: To the current wordToAdd, append first character of a word.
 *			 At the current index, add wordToAdd.
 */

void AddToDictionary(DICTINFO *dictPtr, char *aWord) {
	
	if (dictPtr->wordToAdd) {
		strncat(dictPtr->wordToAdd, aWord, 1);
		dictPtr->dictionary[dictPtr->index] = dictPtr->wordToAdd;
	}
	
	else {
		fprintf(stderr, "Tried to concatenate a word that doesn't exist!\n");
		exit(EXIT_FAILURE);
	}
}

/* Function: Check if current index is greater than max size of dictionary,
 *			 if it is, reset index and dictionary to starting position.
 */

void CheckIndex(DICTINFO *dictPtr) {
	
	if (dictPtr->index == MAXSIZE){
		dictPtr->index = 256;
		Reset(dictPtr, 256);
	}
}

/* Function: Reads NoOfBytes from buffer, 
 *			 Converts these bytes to code and outputs into outptr.
 *			 Adds new words to dictionary.
 */
bool Output(FILE *outptr, int NoOfBytes, char *buffer, DICTINFO *dictPtr) {

	int i = 0;
	uint16_t code = 0;
		
	for (i = 0; i < NoOfBytes; i++) {
		
		// ConvToCode reads byte 3 when getting code from byte 2 and 3.
		if ( i % 3 != 2) {
			
			code = ConvToCode(buffer[i], buffer[i + 1], i % 3);
				
			// if dictionary[code] points to a word
			if (dictPtr->dictionary[code]) {
				
				fputs(dictPtr->dictionary[code], outptr);
					
				// first case after initialising dictPtr
				if (dictPtr->wordToAdd == NULL) {		
					
					if (!CreateNewWord(dictPtr, code))
						break;				
				}
									
				else {
					AddToDictionary(dictPtr, dictPtr->dictionary[code]);
					
					// creating copy of word: dictionary[code]
					if (!CreateNewWord(dictPtr, code))
						break;
						
					dictPtr->index++;	
					CheckIndex(dictPtr);
				}			
			}
			
			// special case when dictionary[code] is not initialised
			else {
				AddToDictionary(dictPtr, dictPtr->wordToAdd);
				fputs(dictPtr->dictionary[code], outptr);
				
				if (!CreateNewWord(dictPtr, code))
					break;
					
				dictPtr->index++;
				CheckIndex(dictPtr);
			}			
		}		
	}
	// makes sure we read through the whole buffer
	if (i == NoOfBytes)
		return true;
	
	else 
		return false;
}

/* Function: decodes inFile, and writes to outFile.
 */
bool Decode(const char *inFile, const char *outFile) {

	bool decoded = false;
	FILE *outptr;
	int n;
	
	FILE *inptr = fopen(inFile, "r" );
	if (inptr == NULL) {
		fprintf(stderr, "Could not open file to read!\n");
		goto done;
	}
	
	outptr = fopen(outFile, "w");
	if (outptr == NULL) {
		fprintf(stderr, "Could not open file to write!\n");
		goto closeRead;
	}
	
	char buffer[BUFFERSIZE];
	DICTINFO dict;
	
	if (!Init(&dict))
		goto end;
	
	while ((n = fread(buffer, 1, BUFFERSIZE, inptr)) != 0) {
	
		// check for read error
		if (n != BUFFERSIZE) {
			
			if (ferror(inptr)) {
				fprintf(stderr, "Error occured when reading %s into buffer!\n", inFile);
				goto end;
			}			
		}
		
		// An encoded file has 3k or 3k + 2 bytes.
		// 3k + 2 only when there are an odd number of codes.
		// since our buffersize is a multiple of 3, 
		// we only read the odd byte on the last loop of our while-loop.
		if (n % 3 == 0) {
			
			if (!Output(outptr, n, buffer, &dict))
				goto end;
		}
		
		else {
			
			if (!Output(outptr, n - 2, buffer, &dict))
				goto end;
			
			// output the odd code
			
			if (dict.dictionary[ConvToCode(buffer[n-2], buffer[n-1], 1)]) 
				fputs(dict.dictionary[ConvToCode(buffer[n-2], buffer[n-1], 1)], outptr);
			
			else {
				AddToDictionary(&dict, dict.wordToAdd);
				fputs(dict.dictionary[ConvToCode(buffer[n-2], buffer[n-1], 1)], outptr);
			}
		}			
	}	
	
	//yay
	decoded = true;
	goto end;
	
	end:
		Reset(&dict, 0);
		fclose(outptr);	
	closeRead:
		fclose(inptr);
	done:
		return decoded;	
}
