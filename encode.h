#include <cstdint>
#include <vector>
#include <string>
#include <cstdio>

using namespace std;

/* Our hashtable will be made up of these entries. */
typedef struct entry {
	uint16_t code;
	string word;
} entry;

/* This keeps track of all our code info during the encoding process */
typedef struct CodeInfo {
	uint16_t currCode;
	char codeBuffer[3];
	int totalCode;
} CodeInfo;

/* Initialises hashtable with characters 0 - 255 and their codes */
void InitTable(vector<entry> *table);

/* Resets hashtable to its initialised form */
void ResetTable(vector<entry> *table);

/* Adds new entry to hashtable with code currCode and word wordToAdd */
void AddToTable(vector<entry> *table, string wordToAdd, CodeInfo *codePtr);

/* Searches table for a word, returns pointer to entry if true, else NULL*/
entry *SearchTable(vector<entry> *table, string wordToSearch);

/* Prints a code to output file */
void PrintCode(CodeInfo *codePtr, FILE *outptr, uint16_t codeToPrint);

/* Encodes inFile, writes to outFile. returns true if successful, else false */
bool Encode(const char *inFile, const char *outFile);
